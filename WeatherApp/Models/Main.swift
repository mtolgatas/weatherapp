//
//  Main.swift
//  WeatherApp
//
//  Created by MUSTAFA TOLGA TAS on 1.05.2020.
//  Copyright © 2020 MUSTAFA TOLGA TAS. All rights reserved.
//

import Foundation

struct Main : Decodable {
    let temp : Double?
    let feels_like : Double?
    let temp_min : Double?
    let temp_max : Double?
    let pressure : Int?
    let humidity : Int?
}
