//
//  Weather.swift
//  WeatherApp
//
//  Created by MUSTAFA TOLGA TAS on 1.05.2020.
//  Copyright © 2020 MUSTAFA TOLGA TAS. All rights reserved.
//

import Foundation

struct Weather : Decodable {
    let id : Int?
    let main : String?
    let description : String?
    let icon : String?
}
