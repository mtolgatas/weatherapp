//
//  DetailViewController.swift
//  WeatherApp
//
//  Created by MUSTAFA TOLGA TAS on 1.05.2020.
//  Copyright © 2020 MUSTAFA TOLGA TAS. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var degreeLabel: UILabel!
    
    // MARK: Variables
    var model : WeatherBase?
    
    // MARK: Override Functions
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareViews()
    }
    
    // MARK: Class Functions
    func prepareViews(){
        guard let model = model else {return}
        locationLabel.text = model.name
        weatherImage.image = UIImage(named: model.weather?.first?.icon ?? "01d")
        degreeLabel.text = "\(kelvinToDegreeString(temp: model.main?.temp ?? 273)) °C"
    }
}
