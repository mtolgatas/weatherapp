//
//  LandingViewController.swift
//  WeatherApp
//
//  Created by MUSTAFA TOLGA TAS on 1.05.2020.
//  Copyright © 2020 MUSTAFA TOLGA TAS. All rights reserved.
//

import UIKit

import CoreLocation

class LandingViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var textField: UITextField!
    
    // MARK: Variables
    let locationManager = CLLocationManager()
    var latitude : String?
    var longitude : String?
    var apiKey : String = ""
    
    
    // MARK: Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        textField.delegate = self
        
        hideKeyboardWhenTappedAround()
    }
    override func viewDidAppear(_ animated: Bool) {
        checkLocationAuth()
    }
    
    // MARK: Class Functions
    func fetchData(){
        guard let lat = latitude else {return}
        guard let lon = longitude else {return}
        
        if apiKey.isEmpty{
            let alert = UIAlertController(title: "Uyarı", message: "API Key boş bırakılamaz", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            _ = WebServiceImp.shared.fetchData(lat: lat, lon: lon, apiKey: apiKey).done({ (result) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
                viewController.model = result
                self.navigationController?.pushViewController(viewController, animated: true)
            }).catch({ (_) in
                let alert = UIAlertController(title: "Uyarı", message: "API Key doğru girilmeli", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            })
        }
    }
    
    func checkLocationAuth() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            locationManager.requestLocation()
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:
            
            let alert = UIAlertController(title: "Uyarı", message: "Konuma izin vermen gerek", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Ayarlar", style: UIAlertAction.Style.default, handler: { (action) in
                self.openSettings()
            }))
            alert.addAction(UIAlertAction(title: "Kapat", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: IBAction Functions
    @IBAction func continueAction(_ sender: Any) {
        textField.endEditing(true)
        fetchData()
    }
    
}

// MARK: Location Extension
extension LandingViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue : CLLocationCoordinate2D = manager.location?.coordinate ?? CLLocationCoordinate2D()
        
        let lat = locValue.latitude
        let lon = locValue.longitude
        
        latitude = String(lat)
        longitude = String(lon)
        
        print("Location Updated With Latitude: \(lat) Longitude: \(lon)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        let alert = UIAlertController(title: "Uyarı", message: "Konum alınırken hata oluştu", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Kapat", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: UITextField Extension
extension LandingViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        apiKey = textField.text ?? ""
    }
}
