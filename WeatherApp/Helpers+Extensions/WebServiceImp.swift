//
//  WebServiceImp.swift
//  WeatherApp
//
//  Created by MUSTAFA TOLGA TAS on 1.05.2020.
//  Copyright © 2020 MUSTAFA TOLGA TAS. All rights reserved.
//

import Alamofire
import PromiseKit

class WebServiceImp {
    static let shared = WebServiceImp()
    
    func fetchData(lat: String, lon: String, apiKey: String) -> Promise<WeatherBase>{
        return Promise{ seal in
            let path = "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=\(apiKey)"
            Alamofire.request(path, method: .get)
                .validate()
                .responseData(){ response in
                    switch response.result{
                    case .success(let weather):
                        let jsonDecoder = JSONDecoder()
                        do{
                            let weather = try jsonDecoder.decode(WeatherBase.self, from: weather)
                            seal.fulfill(weather)
                        } catch {
                            print("Response : Weather couldn't get:")
                            print(error)
                            seal.reject(error)
                        }
                    case .failure(let error):
                        print("Response : Weather couldn't get:")
                        print(error)
                        seal.reject(error)
                    }
            }
        }
    }
}
